FROM ubuntu:xenial

RUN apt-get update && apt-get --quiet install --yes cmake gcc g++ git sudo curl openssl libssl-dev libpng-dev libgles2-mesa-dev valgrind unzip ninja-build libsqlite3-dev libboost-all-dev gcovr doxygen  \
  && rm -rf /var/lib/apt/lists/*
